# cf-cn-mes

This repository contains the specific code for the paper "Influence of Adaptive Coupling Points onCoalition Formation in Multi-Energy Systems"

# Install

```
pip install -r requirements.txt
pip install -e .
pip install secmes/.
```

# Executing the simulation

```
python cfcnmes/adaption.py
```


# Troubleshooting

The used pandapipes version 0.6.0 contains a bug which affects some networks. If an exception in _relevant_nets occurs, replace the methods body with:

```
    net_names = dict()

    nns = []
    levelorder = np.array(levelorder)
    rel_levelorder_multi = levelorder[:, 1].__eq__(multinet)
    controller = levelorder[rel_levelorder_multi, 0]
    nns += [ctrl.get_all_net_names() for ctrl in controller]
    rel_levelorder_multi_w = np.copy(rel_levelorder_multi)

    for net_name in multinet['nets'].keys():
        net = multinet['nets'][net_name]
        rel_levelorder = levelorder[:, 1].__eq__(net)
        level_excl = [False if net_name not in nn else True for nn in nns]
        
        rel_levelorder_multi_w[rel_levelorder_multi] = level_excl 
        net_names[net_name] = np.maximum(rel_levelorder_multi_w, rel_levelorder)

    return net_names
```